# openpit-audio-player

Single stage audio player for Open Pit events. Includes a play/pause button and volume control.

The component will take on font size/color/family/etc from its parent element, but there are also CSS variables you can use to customize sizing and colors.

## CSS controls

The sizing of various elements in the player can be set with the following CSS variables on the root element (`<op-audio-player>`):

| Property                      | Description                                 | Type   | Default        |
|-------------------------------|---------------------------------------------|--------|----------------|
| `--control-button-size`       | The size of the play/pause buttton          | Length | `4em`          |
| `--volume-slider-thumb-color` | The color of the thumb on the volume slider | Color  | `currentColor` |
| `--volume-slider-track-color` | The color of the track on the volume slider | Color  | `currentColor` |
| `--volume-slider-width`       | The width of the volume slider              | Length | `8em`          |
| `--volume-slider-thumb-size`  | The size of the thumb on the volume slider  | Length | `1.5em`        |



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description                                                       | Type      | Default                                  |
| --------- | --------- | ----------------------------------------------------------------- | --------- | ---------------------------------------- |
| `playing` | `playing` | Controls audio playback — if set to true initially, will autoplay | `boolean` | `false`                                  |
| `src`     | `src`     | The audio stream url (set to Open Pit's audio stream by default)  | `string`  | `"https://edge.mixlr.com/channel/iumlt"` |
| `volume`  | `volume`  | The initial volume level (range 0-1)                              | `number`  | `1`                                      |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
