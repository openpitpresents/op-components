import {
  Component,
  Prop,
  h,
  Host,
  Watch,
  State,
  Event,
  EventEmitter,
} from "@stencil/core";

@Component({
  tag: "op-audio-player",
  styleUrl: "op-audio-player.css",
  shadow: true,
})
export class OPAudioPlayer {
  /**
   * The audio stream url (set to Open Pit's audio stream by default)
   */
  @Prop() src: string = "https://edge.mixlr.com/channel/iumlt";

  /**
   * The initial volume level (range 0-1)
   */
  @Prop({ mutable: true }) volume = 1;

  /**
   * Controls audio playback — if set to true initially, will autoplay
   */
  @Prop({ mutable: true, reflect: true }) playing = false;

  @State() nowPlaying: string | null = null;

  @Event() playingStateChanged: EventEmitter;

  audio!: HTMLAudioElement;
  interval!: number;

  componentDidLoad() {
    if (this.playing) {
      this.audio.play();
      this.audio.muted = false;
    } else {
      this.audio.muted = true;
    }
    const savedVolume = sessionStorage.getItem("op-audio-player-volume");
    if (savedVolume) {
      this.volume = JSON.parse(savedVolume);
    }
    this.audio.volume = this.volume;
    this.getNowPlaying();
    this.interval = window.setInterval(this.getNowPlaying.bind(this), 5000);
    // this.audio.addEventListener("error", () => {
    //   this.audio.load();
    //   this.audio.play();
    // });
    this.audio.addEventListener("stalled", () => {
      this.audio.load();
      this.audio.play();
    });
    this.audio.addEventListener("suspend", () => {
      this.audio.load();
      this.audio.play();
    });
    this.audio.addEventListener("pause", () => {
      this.audio.play();
    });
  }

  componentDidUnload() {
    window.clearInterval(this.interval);
  }

  @Watch("volume")
  handleVolumeChange(newVolume, oldVolume) {
    if (typeof newVolume !== "number") {
      this.volume = oldVolume;
      throw new Error(`The volume provided (${newVolume}) is not a number`);
    }
    if (newVolume > 1 || newVolume < 0) {
      this.volume = Math.max(0, Math.min(1, newVolume));
      throw new Error(
        `The volume provided (${newVolume}) is outside the range [0, 1]`
      );
    }
    this.audio.volume = this.volume;
  }

  @Watch("playing")
  handlePlayingChange() {
    if (this.playing) {
      this.audio.muted = false;
      this.audio.play();
      console.debug("Audio Playing");
    } else {
      this.audio.muted = true;
      console.debug("Audio Paused");
    }
  }

  private handleVolumeChangeEvent(event) {
    this.audio.play();
    const volume = Math.max(0, Math.min(1, event.target.value));
    this.volume = volume;
    sessionStorage.setItem("op-audio-player-volume", JSON.stringify(volume));
    // this.audio.volume = this.volume;
  }

  private handlePlayingChangeEvent() {
    this.audio.play();
    this.playing = !this.playing;
    this.playingStateChanged.emit(this.playing);
  }

  private async getNowPlaying() {
    try {
      const user = await fetch("https://api.mixlr.com/users/7046821");
      const userState = await user.json();
      if (userState.is_live) {
        const broadcastId = userState.broadcast_ids[0];
        const broadcast = await fetch(
          `https://api.mixlr.com/broadcasts/${broadcastId}`
        );
        const broadcastState = await broadcast.json();
        this.nowPlaying = broadcastState.title.replace(/ on Mixlr$/i, "");
      } else {
        this.nowPlaying = null;
      }
    } catch (_) {
      this.nowPlaying = null;
    }
  }

  render() {
    return (
      <Host>
        <div class="audio-player-container">
          <button onClick={this.handlePlayingChangeEvent.bind(this)}>
            <div class="svg">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path
                  fill="currentColor"
                  d={
                    this.playing
                      ? "M256 48C141.31 48 48 141.31 48 256s93.31 208 208 208 208-93.31 208-208S370.69 48 256 48zm-32 288h-32V176h32zm96 0h-32V176h32z"
                      : "M256 48C141.31 48 48 141.31 48 256s93.31 208 208 208 208-93.31 208-208S370.69 48 256 48zm-56 296V168l144 88z"
                  }
                />
              </svg>
            </div>
            <span part="label">{this.playing ? "Pause" : "Play"}</span>
          </button>
          <label htmlFor="volume">
            <div class="input-wrapper">
              <input
                name="volume"
                type="range"
                min="0"
                max="1"
                step="any"
                value={this.volume}
                onInput={this.handleVolumeChangeEvent.bind(this)}
              />
            </div>
            <span part="label">Volume</span>
          </label>
          <audio
            src={this.src}
            autoplay={true}
            ref={(el) => {
              this.audio = el;
            }}
          />
        </div>
        {this.nowPlaying !== null ? (
          <div class="now-playing">
            Now Playing: <strong>{this.nowPlaying}</strong>
          </div>
        ) : (
          <div class="now-playing">Nothing Playing.</div>
        )}
      </Host>
    );
  }
}
